import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import '../../styles/content.css';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      year: new Date().getFullYear()
    }
  }

  render() {
    return(
      <div className="footer__wrapper">
        <Container>
          <Row>
            <Col>
              <ul className="footer__phone-container">
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="tel:+436607474372">
                      <FontAwesome
                        className='footer__icon'
                        name='phone'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <p className="footer__text">+436607474372</p>
                  </div>
                </li>
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="skype:iuic.info?call">
                      <FontAwesome
                        className='footer__icon'
                        name='skype'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <p className="footer__text">iuic.info</p>
                  </div>
                </li>
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="mailto:iuic.info@gmail.com">
                      <FontAwesome
                        className='footer__icon'
                        name='envelope'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <p className="footer__text">iuic.info@iuic.info</p>
                  </div>
                </li>
              </ul>
              <div className="center-block tria-box tria-box-green text-center">
                <div className="footer-logo">
                  <p className="footer-logo__caption">IUIC</p>
                </div>
                <p className="f10 text-light-green mb-0 footer__copywrite">&copy; 2013 - {this.state.year} г. IUIC INFO O&#220; </p>
              </div>
              <ul className="footer__social-icons-container">
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="https://www.linkedin.com/company/18209598/" target="_blank" rel="noopener noreferrer">
                      <FontAwesome
                        className='footer__icon'
                        name='linkedin'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <a className="footer__text" href="https://www.linkedin.com/company/18209598/">linkedin.com/iuic</a>
                  </div>
                </li>
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="https://www.facebook.com/iuic.info/" target="_blank" rel="noopener noreferrer">
                      <FontAwesome
                        className='footer__icon'
                        name='facebook'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <a className="footer__text" href="https://www.facebook.com/iuic.info/">fb.com/iuic.info</a>
                  </div>
                </li>
                <li className="footer__list-item">
                  <div className="footer__text-wrapper">
                    <a className="footer__link" href="https://vk.com/iuicinfo" target="_blank" rel="noopener noreferrer">
                      <FontAwesome
                        className='footer__icon'
                        name='vk'
                        size='2x'>
                      </FontAwesome>
                    </a>
                    <a href="https://vk.com/iuicinfo" className="footer__text">vk.com/iuicinfo</a>
                  </div>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Content;