import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import ReactPhoneInput from 'react-phone-input';
import axios from 'axios';
import LocalizedStrings from 'react-localization';
import '../../styles/feedback-modal.css';

  const strings = new LocalizedStrings({
    en: {
      header: "To get a consultation",
      substring: "After sending the application, our specialist will contact you",
      fields: {
        first: "Name",
        second: "Phone",
        third: "Email",
        four: {
          header: "Select a service",
          firstOption: "Site development",
          secondOption: "Developing a mobile application"
        },
        five: "Comment"
      },
      button: "Send",
      thanks: 'Thank you!',
      contact: 'We will contact you soon.'
    },
    de: {
      header: "Ratschläge bekommen",
      substring: "Nach dem Absenden der Bewerbung wird sich unser Spezialist mit Ihnen in Verbindung setzen",
      fields: {
        first: "Name",
        second: "Telefon",
        third: "Email",
        four: {
          header: "Wählen Sie einen Dienst aus",
          firstOption: "Website-Entwicklung",
          secondOption: "Entwicklung einer mobilen Anwendung"
        },
        five: "Kommentar"
      },
      button: "Senden",
      thanks: 'Danke!',
      contact: 'Wir werden uns bald mit Ihnen in Verbindung setzen.'
    },
    ru: {
      header: "Получить консультацию",
      substring: "После отправки заявки с Вами свяжется наш специалист",
      fields: {
        first: "Имя",
        second: "Телефон",
        third: "Эл. адрес",
        four: {
          header: "Выберите услугу",
          firstOption: "Разработка сайта",
          secondOption: "Разработка мобильного приложения"
        },
        five: "Комментарий"
      },
      button: "Отправить",
      thanks: 'Спасибо!',
      contact: 'Мы свяжемся с Вами в ближайшее время.'
    }
  });

class FeedbackModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      phone: '',
      name: '',
      sent: false,
      validName: false,
      validPhone: false
    };

    this.toggle = this.toggle.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
      sent: false,
      validName: false,
      validPhone: false
    });
  }

  sendRequest() {
    let form = document.querySelector("#form"),
      _this = this,
      fd = new FormData(form),
      action = "https://docs.google.com/forms/d/e/1FAIpQLSdUdL-quP_bcRkvO_S7AFfqZx_BbFX6NgfAxvEL15FV0m0GlQ/formResponse";

    axios.post(action, fd)
      .then((res) => {

        _this.setState({
          sent: !_this.state.sent
        });

        console.log(res)
      })
      .catch((err) => {

        _this.setState({
          sent: !_this.state.sent
        });

        console.log(err);
      })
  }

  handleOnChange (value) {
    if (value.replace(/\D/g,'').length > 8) {
      this.setState({
        phone: value,
        validPhone: true
      });
    } else {
      this.setState({
        phone: value,
        validPhone: false
      });
    }
  }

  typeName(event) {
    if (event.target.value) {
      this.setState({
        name: event.target.value,
        validName: true
      })
    } else {
      this.setState({
        name: event.target.value,
        validName: false
      })
    }
  }

  componentDidMount() {
    const _this = this;
    let showModalButton = document.querySelector("#show-modal-btn");
    showModalButton.addEventListener('click', function() {
      _this.setState({
        modal: !_this.state.modal
      })
    })
  }

  render() {

    strings.setLanguage(this.props.lang);
    let formValid =  this.state.validName && this.state.validPhone;
    let bodyModal;
    if (this.state.sent) {
      bodyModal =
        <ModalBody>
          <div className="modal__thanks">
            <div className="modal__thanks-content">
              <h2>{strings.thanks}</h2>
              <p>{strings.contact}</p>
            </div>
          </div>
      </ModalBody>;
    } else {
      bodyModal =
        <div>
          <ModalBody>
            <Form id="form">
              <FormGroup>
                <Label for="name">{strings.fields.first} <span className="modal__require"> *</span></Label>
                <Input type="text" name="entry.992559263" id="name" onChange={this.typeName.bind(this)}/>
              </FormGroup>
              <FormGroup>
                <Label>{strings.fields.second} <span className="modal__require"> *</span></Label>
                <ReactPhoneInput defaultCountry={'us'} onChange={this.handleOnChange}></ReactPhoneInput>
                <input type="hidden" name="entry.1422317550" value={this.state.phone} />
              </FormGroup>
              <FormGroup>
                <Label for="email">{strings.fields.third}</Label>
                <Input type="email" name="entry.2090360227" id="email" />
              </FormGroup>
              <FormGroup>
                <Label for="text-area">{strings.fields.five}</Label>
                <Input type="textarea" name="entry.1233571765" id="text-area" />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button disabled={!formValid} className="modal__submit" onClick={this.sendRequest.bind(this)}>{strings.button}</Button>
          </ModalFooter>
        </div>;
    }
    return (
      <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>
          {strings.header}
          <div className="modal__caption">{strings.substring}</div>
        </ModalHeader>
        {bodyModal}
      </Modal>
    );
  }
}

export default FeedbackModal;