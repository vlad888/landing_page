import React, { Component } from 'react';
import Slider from 'react-slick';
import '../../styles/rumor.css';

class IuicLifeSlider extends Component {
  render() {

    let settings = {
          accessibility: true,
          autoplay: true,
          dots: false,
          singleItem: true,
          speed: 750,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true
        };

    return(
      <Slider {...settings}>
        <div className="">
          <img className="iuic-life__img" src="/iuic-life_img/iuic-life1.jpg" alt="" />
        </div>
        <div className="">
          <img className="iuic-life__img" src="/iuic-life_img/iuic-life2.jpg" alt="" />
        </div>
        <div className="">
          <img className="iuic-life__img" src="/iuic-life_img/iuic-life3.jpg" alt="" />
        </div>
        <div className="">
          <img className="iuic-life__img" src="/iuic-life_img/iuic-life4.jpg" alt="" />
        </div>
      </Slider>
    );
  }
}

export default IuicLifeSlider;