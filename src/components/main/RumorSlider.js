import React, { Component } from 'react';
import Slider from 'react-slick';
import '../../styles/rumor.css';

class RumorSlider extends Component {
  render() {

    let settings = {
          accessibility: true,
          autoplay: true,
          dots: false,
          singleItem: true,
          speed: 750,
          slidesToShow: 1,
          slidesToScroll: 1
        };

    return(
      <Slider {...settings}>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor1.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor2.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor3.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor4.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor5.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor6.png" alt="rumor-app-screenshot" />
        </div>
        <div className="">
          <img className="rumor__img" src="/rumor_img/rumor7.png" alt="rumor-app-screenshot" />
        </div>
      </Slider>
    );
  }
}

export default RumorSlider;