import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Snap from 'snapsvg-cjs';
import LocalizedStrings from 'react-localization';

const strings = new LocalizedStrings({
  en: {
    firstColumn: {
      header: "development",
      caption: "of mobile applications",
      text: "for iOS and Android platforms"
    },
    secondColumn: {
      header: "creating",
      caption: "unique tools",
      text: "For your company"
    },
    thirdColumn: {
      header: "support",
      caption: "in application management",
      text: "Development of applications in accordance with business objectives"
    },
    fourColumn: {
      header: "analytics",
      caption: "research and analysis",
      text: "We examine particular features in the chosen field, determine the target audience and analyze the strategy of competitors"
    }
  },
  de: {
    firstColumn: {
      header: "Entwicklung",
      caption: "von mobilen Anwendungen",
      text: "Für iOS- und",
      textPathTwo: "Android-Betriebssysteme"
    },
    secondColumn: {
      header: "Erstellen",
      caption: "von einzigartigen",
      text: "Tools für Ihr Unternehmen oder Ihr Mobilfunkgeschäft"
    },
    thirdColumn: {
      header: "Unterstützung",
      caption: "bei der Verwaltung von Anwendungen",
      text: "Entwicklung von Anwendungen im Einklang mit den Geschäftszielen"
    },
    fourColumn: {
      header: "analytik",
      caption: "Studie, Analyse",
      text: "Wir studieren die Besonderheiten der gewählten Geschäftssphäre, die Zielgruppe, wir analysieren die Strategie der direkten Konkurrenten"
    }
  },
  ru: {
    firstColumn: {
      header: "разработка",
      caption: "мобильных приложений",
      text: "Под операционные системы",
      textPathTwo: "iOS и Android"
    },
    secondColumn: {
      header: "создание",
      caption: "уникальных инструментов",
      text: "Для Вашей компании или мобильного магазина"
    },
    thirdColumn: {
      header: "поддержка",
      caption: "в управлении приложениями",
      text: "Развитие приложений в соответствии с бизнес-задачами"
    },
    fourColumn: {
      header: "аналитика",
      caption: "изучение и анализ",
      text: "Мы изучаем особенности выбранной сферы бизнеса, целевой аудитории, анализируем стратегию работы прямых конкурентов"
    }
  }
});

class Servises extends Component {
  constructor(props) {
    super(props);
    this.state = {
      speed: 300,
      path: undefined,
      pathConfig: {
        from: "M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z",
        to: "M 0,0 0,38 90,58 180.5,38 180,0 z"
      }
    }
  }

  mousenter(event) {
    let svg = event.currentTarget.querySelector("svg"),
    s = Snap( svg ),
    path = s.select( 'path' );

    this.setState({
      path: path
    });

    path.animate( { 'path' : this.state.pathConfig.to }, this.state.speed );
  }

  mouseleave() {
    this.state.path.animate( { 'path' : this.state.pathConfig.from }, this.state.speed );
  }

  render() {
    strings.setLanguage(this.props.lang);
    return(
      window.innerWidth > 1050
      //Desktop version
      ? <div className="servises" id="servises">
          <Container className="servises--custom-offset">
            <Row>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item"
                  onMouseEnter={ this.mousenter.bind(this) }
                  onMouseLeave={ this.mouseleave.bind(this) }>
                  <figure className="servises__figure">
                    <div className="servises__item--background servises__background--bg1"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item--svg">
                      <path className="servises__item--svg-path" d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <div className="servises__separator"></div>
                        <h3 className="servises__header">{strings.firstColumn.header}</h3>
                        <p className="servises__text">{strings.firstColumn.caption}</p>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__hidden-text">{strings.firstColumn.text}  <br/> {strings.firstColumn.textPathTwo}</div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item"
                  onMouseEnter={ this.mousenter.bind(this) }
                  onMouseLeave={ this.mouseleave.bind(this) }>
                  <figure className="servises__figure">
                    <div className="servises__item--background servises__background--bg2"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item--svg">
                      <path className="servises__item--svg-path" d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <div className="servises__separator"></div>
                        <h3 className="servises__header">{strings.secondColumn.header}</h3>
                        <p className="servises__text">{strings.secondColumn.caption}</p>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__hidden-text">{strings.secondColumn.text} <br/> {strings.secondColumn.textPathTwo} <br/> {strings.secondColumn.textPathThree} </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item"
                  onMouseEnter={ this.mousenter.bind(this) }
                  onMouseLeave={ this.mouseleave.bind(this) }>
                  <figure className="servises__figure">
                    <div className="servises__item--background servises__background--bg3"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item--svg">
                      <path className="servises__item--svg-path" d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <div className="servises__separator"></div>
                        <h3 className="servises__header">{strings.thirdColumn.header}</h3>
                        <p className="servises__text">{strings.thirdColumn.caption}</p>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__hidden-text">{strings.thirdColumn.text} <br/> {strings.thirdColumn.textPathTwo} <br/> {strings.thirdColumn.textPathThree}</div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item"
                  onMouseEnter={ this.mousenter.bind(this) }
                  onMouseLeave={ this.mouseleave.bind(this) }>
                  <figure className="servises__figure">
                    <div className="servises__item--background servises__background--bg4"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item--svg">
                      <path className="servises__item--svg-path" d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <div className="servises__separator"></div>
                        <h3 className="servises__header">{strings.fourColumn.header}</h3>
                        <p className="servises__text">{strings.fourColumn.caption}</p>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__hidden-text">{strings.fourColumn.text}</div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      //Mobile version
      : <div className="servises-mobile" id="servises">
          <Container className="servises--custom-offset">
            <Row>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item-mobile">
                  <figure className="servises__figure">
                    <div className="servises__item-mobile--background servises__background--bg1"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item-mobile--svg">
                      <path className="servises__item-mobile--svg-path" d="M 0,0 0,38 90,58 180.5,38 180,0 z"/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <h3 className="servises__header">{strings.firstColumn.header}</h3>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__mobile-text">{strings.firstColumn.text} <br/> {strings.firstColumn.textPathTwo} <br/> {strings.firstColumn.textPathThree} </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
              <Col xs="12" md="6" lg="3">
                <div
                  className="servises__item-mobile">
                  <figure className="servises__figure">
                    <div className="servises__item-mobile--background servises__background--bg2"></div>
                    <svg
                      viewBox="0 0 180 320"
                      preserveAspectRatio="none"
                      className="servises__item-mobile--svg">
                      <path className="servises__item-mobile--svg-path" d="M 0,0 0,38 90,58 180.5,38 180,0 z"/>
                    </svg>
                    <figcaption className="servises__figcaption">
                      <div className="servises__content">
                        <h3 className="servises__header">{strings.secondColumn.header}</h3>
                      </div>
                      <div className="servises__button-container">
                        <div className="servises__mobile-text">
                        {strings.secondColumn.text} <br/> {strings.secondColumn.textPathTwo} <br/> {strings.secondColumn.textPathThree} </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </Col>
              <Col xs="12" md="6" lg="3">
              <div
              className="servises__item-mobile">
              <figure className="servises__figure">
                <div className="servises__item-mobile--background servises__background--bg3"></div>
                <svg
                  viewBox="0 0 180 320"
                  preserveAspectRatio="none"
                  className="servises__item-mobile--svg">
                  <path className="servises__item-mobile--svg-path" d="M 0,0 0,38 90,58 180.5,38 180,0 z"/>
                </svg>
                <figcaption className="servises__figcaption">
                  <div className="servises__content">
                    <h3 className="servises__header">{strings.thirdColumn.header}</h3>
                  </div>
                  <div className="servises__button-container">
                    <div className="servises__mobile-text">{strings.thirdColumn.text} <br/> {strings.thirdColumn.textPathTwo} <br/> {strings.thirdColumn.textPathThree} </div>
                  </div>
                </figcaption>
              </figure>
            </div>
              </Col>
              <Col xs="12" md="6" lg="3">
              <div
              className="servises__item-mobile">
              <figure className="servises__figure">
                <div className="servises__item-mobile--background servises__background--bg4"></div>
                <svg
                  viewBox="0 0 180 320"
                  preserveAspectRatio="none"
                  className="servises__item-mobile--svg">
                  <path className="servises__item-mobile--svg-path" d="M 0,0 0,38 90,58 180.5,38 180,0 z"/>
                </svg>
                <figcaption className="servises__figcaption">
                  <div className="servises__content">
                    <h3 className="servises__header">{strings.fourColumn.header}</h3>
                  </div>
                  <div className="servises__button-container">
                    <div className="servises__mobile-text">{strings.fourColumn.text} <br/> {strings.fourColumn.textPathTwo} <br/> {strings.fourColumn.textPathThree} </div>
                  </div>
                </figcaption>
              </figure>
            </div>
              </Col>
            </Row>
          </Container>
        </div>
    );
  }
}

export default Servises;