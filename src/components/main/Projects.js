import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Slider from 'react-slick';
import RumorSlider from './RumorSlider';
import IuicChatSlider from './IuicChatSlider';
import IuicLifeSlider from './IuicLifeSlider';
import Media from 'react-media';
import LocalizedStrings from 'react-localization';
import '../../styles/projects.css';

  const strings = new LocalizedStrings({
    en: {
      rumor: {
        text: {
          firstPath: "Audio podcasts and photos of eyewitnesses from different parts of the city."
        }
      },
      iuicLife: {
        header: "Photo events of the current city life",
        text: {
          firstPath: "Unfold the reality in each city with photos.",
          secondPath: "Make and upload your own photos to contribute to the lifestyle of your city."
        }
      },
      iuicChat: {
        header: "The world of open communication",
        text: {
          firstPath: "Private and public chats everywhere around the city. Meet new people spontaneously in your city or even in the whole world!",
        }
      }
    },
    de: {
      rumor: {
        header: "Rumor - beliebtes Gerücht",
        text: {
          firstPath: "Sprachnachrichten und Fotos von Augenzeugen von Orten der Ereignisse in allen Städten der Welt."
        }
      },
      iuicLife: {
        header: "Fotos von Veranstaltungen aus dem wirklichen Leben der Städte",
        text: {
          firstPath: "Zeige die Realität in jedem",
          secondPath: "Stadt mit Fotos, verlassen",
          thirdPath: "die Spur des Autors von interessanten Ereignissen in",
          fourPath: "jede Stadt in der Welt."
        }
      },
      iuicChat: {
        header: "Die Welt der offenen Kommunikation",
        text: {
          firstPath: "Chats, öffentlich und persönlich, überall",
          secondPath: "Unerwartete Treffen und neue Leute überall auf der Welt treffen.",
          thirdPath: "Schnelle PR"
        }
      }
    },
    ru: {
      rumor: {
        header: "Rumor - народная молва",
        text: {
          firstPath: "Голосовые новости и фото очевидцев с мест событий во всех городах мира.",
          secondPath: ' ',
          thirdPath: ' '
        }
      },
      iuicLife: {
        header: "Фото событий из реальной жизни городов",
        text: {
          firstPath: "Раскрывайте действительность в каждом",
          secondPath: "городе при помощи фотографий, оставляйте",
          thirdPath: "свой авторский след интересных событий в",
          fourPath: "любом городе мира."
        }
      },
      iuicChat: {
        header: "Мир открытого общения",
        text: {
          firstPath: "Чаты, публичные и личные, повсюду",
          secondPath: "Неожиданные встречи и знакомства с новыми людьми в любой точке мира.",
          thirdPath: "Быстрый PR"
        }
      }
    }
  });

function SampleNextArrow(props) {
  const { onClick } = props
  return (
    <div
      className="slider-button slider-button-next"
      onClick={onClick}>
      <span className="projects__arrow projects__arrow--right"></span>
    </div>
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="slider-button slider-button-prev"
      onClick={onClick}>
      <span className="projects__arrow projects__arrow--left"></span>
    </div>
  );
}

class Projects extends Component {
  render() {

    let settings = {
          dots: false,
          singleItem: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          autoplay: true
        };

    strings.setLanguage(this.props.lang);

    return(
      <div className="projects" id="projects">
        <Container fluid>
          <Row>
            <Col className="no-padding">
              <Media query="(min-width: 767px)">
              {matches => matches ? (
                <Slider {...settings}>
                    <div className="projects__item rumor-bg">
                      <div className="projects__phone">
                        <div className="projects__photos">
                          <RumorSlider />
                        </div>
                      </div>
                      <div className="project__caption caption">
                        <div className="caption__wrapper">
                          <h4 className="caption__header">rumor</h4>
                          <p className="caption__text">{strings.rumor.header}</p>
                          <p className="caption__text">
                            {strings.rumor.text.firstPath} <br />
                            {strings.rumor.text.secondPath} <br />
                            {strings.rumor.text.thirdPath}
                          </p>
                        </div>
                          <div className="caption__icons">
                            <a className="caption__link" href="">
                              <img className="app-store__icon" src="/app-store.png" alt="" />
                            </a>
                            <a className="caption__link" href="">
                              <img className="google-play__icon" src="/google-play.png" alt="" />
                            </a>
                          </div>
                      </div>
                    </div>
                  <div className="projects__item iuic-chat-bg iuic-chat">
                    <div className="iuic-chat__phone">
                      <div className="iuic-chat__photos">
                        <IuicChatSlider />
                      </div>
                    </div>
                    <div className="iuic-chat__caption caption">
                      <div className="caption__wrapper">
                        <h4 className="iuic-chat-caption__header">iuic chat</h4>
                        <p className="iuic-chat-caption__text">{strings.iuicChat.header}</p>
                        <p className="iuic-chat-caption__text">
                          {strings.iuicChat.text.firstPath} <br />
                          {strings.iuicChat.text.secondPath} <br />
                          {strings.iuicChat.text.thirdPath}
                        </p>
                      </div>
                        <div className="caption__icons">
                          <a className="caption__link"
                            href="https://itunes.apple.com/ru/app/iuic-chat/id978796876?mt=8"
                            target="_blank"
                            rel="noopener noreferrer">
                            <img className="app-store__icon" src="/app-store.png" alt="" />
                          </a>
                          <a className="caption__link"
                            href="https://play.google.com/store/apps/details?id=com.fivecraft.iuic_android&hl=ru"
                            target="_blank"
                            rel="noopener noreferrer">
                            <img className="google-play__icon" src="/google-play.png" alt="" />
                          </a>
                        </div>
                    </div>
                  </div>
                  <div className="projects__item iuic-life-bg">
                    <div className="iuic-life__phone">
                      <div className="iuic-life__photos">
                        <IuicLifeSlider />
                      </div>
                    </div>
                    <div className="iuic-life__caption caption">
                      <div className="caption__wrapper">
                        <h4 className="iuic-life-caption__header">iuic life</h4>
                        <p className="iuic-life-caption__text">{strings.iuicLife.header}</p>
                        <p className="iuic-life-caption__text">
                          {strings.iuicLife.text.firstPath} <br />
                          {strings.iuicLife.text.secondPath} <br />
                          {strings.iuicLife.text.thirdPath} <br />
                          {strings.iuicLife.text.fourPath}
                        </p>
                      </div>
                        <div className="caption__icons">
                          <a className="caption__link"
                            href="https://itunes.apple.com/ru/app/iuic-life/id1026700832?mt=8"
                            target="_blank"
                            rel="noopener noreferrer">
                            <img className="app-store__icon" src="/app-store.png" alt="" />
                          </a>
                          <a className="caption__link"
                            href="https://play.google.com/store/apps/details?id=life.iuic.android&hl=ru"
                            target="_blank"
                            rel="noopener noreferrer">
                            <img className="google-play__icon" src="/google-play.png" alt="" />
                          </a>
                        </div>
                    </div>
                  </div>
                </Slider>
                ) : (
                  null
                )}
              </Media>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Projects;