import React, { Component } from 'react';
import Slider from 'react-slick';
import '../../styles/rumor.css';

class IuicChatSlider extends Component {
  render() {

    let settings = {
          accessibility: true,
          autoplay: true,
          dots: false,
          singleItem: true,
          speed: 750,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true
        };

    return(
      <Slider {...settings}>
        <div className="iuic-chat__slider-item">
          <img className="iuic-chat__img" src="/iuic-chat_img/iuic-chat1.png" alt="" />
        </div>
        <div className="iuic-chat__slider-item">
          <img className="iuic-chat__img" src="/iuic-chat_img/iuic-chat2.png" alt="" />
        </div>
        <div className="iuic-chat__slider-item">
          <img className="iuic-chat__img" src="/iuic-chat_img/iuic-chat3.png" alt="" />
        </div>
        <div className="iuic-chat__slider-item">
          <img className="iuic-chat__img" src="/iuic-chat_img/iuic-chat4.png" alt="" />
        </div>
        <div className="iuic-chat__slider-item">
          <img className="iuic-chat__img" src="/iuic-chat_img/iuic-chat5.png" alt="" />
        </div>
      </Slider>
    );
  }
}

export default IuicChatSlider;