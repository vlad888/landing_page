import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import LocalizedStrings from 'react-localization';
import '../../styles/feedback.css';

  const strings = new LocalizedStrings({
    en: {
      title: "Main stages of cooperation",
      headers: {
        first: {
          header: "Request",
          caption: "Client sends a request on the website"
        },
        second: {
          header: "Consultation",
          caption: "Our manager contacts the client and discusses all the project details"
        },
        third: {
          header: "Commercial offer",
          caption: "Client receives a commercial offer from us with the price and the date when the project will be completed"
        },
        four: {
          header: "Agreement",
          caption: "We sign up an agreement with terms and conditions and begin the working process"
        }
      },
      button: "Send request"
    },
    de: {
      title: "Hauptphasen der Zusammenarbeit",
      headers: {
        first: {
          header: "Bestellen",
          caption: "Sie füllen aus und senden ein Formular auf unserer Website"
        },
        second: {
          header: "Konsultation",
          caption: "Unser Manager wird sich mit Ihnen in Verbindung setzen und die Grunddaten der Bestellung besprechen"
        },
        third: {
          header: "Angebot",
          caption: "Sie erhalten ein kommerzielles Angebot, das die Kosten und die Frist für das Projekt beinhaltet"
        },
        four: {
          header: "Vertrag",
          caption: "Wir schließen einen offiziellen Vertrag ab und beginnen mit der Arbeit am Projekt"
        }
      },
      button: "Anfrage senden"
    },
    ru: {
      title: "Основные этапы сотрудничества",
      headers: {
        first: {
          header: "Заявка",
          caption: "Вы заполняете и отправляете анкету на нашем сайте"
        },
        second: {
          header: "Консультация",
          caption: "Наш менеджер свяжется с вами и обсудит основные детали заказа"
        },
        third: {
          header: "Предложение",
          caption: "Вы получите коммерческое предложение, в котором указана стоимость и срок сдачи проекта"
        },
        four: {
          header: "Договор",
          caption: "Мы заключаем официальный договор и начинаем работу по проекту"
        }
      },
      button: "Отправить запрос"
    }
  });

class Feedback extends Component {
  render() {

    strings.setLanguage(this.props.lang);

    return(
      <section className="how-order" id="contacts">
        <Container>
          <div className="title-3">{strings.title}</div>
          <Row>
            <Col xs="12" sm="6" lg="3">
              <h4 className="how-order__header--h4">1. {strings.headers.first.header}</h4>
              <p className="how-order__text">{strings.headers.first.caption}</p>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <h4 className="how-order__header--h4">2. {strings.headers.second.header}</h4>
              <p className="how-order__text">{strings.headers.second.caption}</p>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <h4 className="how-order__header--h4">3. {strings.headers.third.header}</h4>
              <p className="how-order__text">{strings.headers.third.caption}</p>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <h4 className="how-order__header--h4">4. {strings.headers.four.header}</h4>
              <p></p>
              <p className="how-order__text">{strings.headers.four.caption}</p>
              <p></p>
            </Col>
          </Row>
          <a className="button blue button-blue-effect" id="show-modal-btn">{strings.button}</a>
        </Container>
      </section>
    );
  }
}

export default Feedback;