import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import LocalizedStrings from 'react-localization';
import '../../styles/about-caption.css';

const strings = new LocalizedStrings({
  en: {
    header: {
      pathOne: "Get high-quality mobile/web applications",
      pathTwo: "at",
      strong: "IUIC"
    },
    body: {
      firstTextParagraph: "If you are looking for effective tools to increase your sales, contact us for mobile/web development. Out experts will make an application that will serve as an indispensable assistant for your clients.",
      secondTextParagpaph: "We offer application development services for iOS/Android in different fields such as e-commerce, services, online mass media, etc. We also offer development for young startups to make your project recognizable and to gain trust and loyalty of the target audience."
    }
  },
  de: {
    header: {
      pathOne: "Bestellen Sie qualitativ hochwertige",
      pathTwo: "Anwendungen im Unternehmen",
      strong: "IUIC"
    },
    body: {
      firstTextParagraph: "Wenn Sie nach neuen, effizienten Verkaufswerkzeugen suchen, bestellen Sie die mobile Anwendungsentwicklung von der IUIC. Unsere Spezialisten bereiten eine Anwendung vor, die für Ihre Kunden ein unverzichtbares Werkzeug wird!",
      secondTextParagpaph: "Wir bieten die Entwicklung von Anwendungen für alle Bereiche auf Plattformen - iOS und Android, wie: Service-Industrie, E-Commerce, Online-Medien, Verlage. Wir empfehlen auch, dass Sie eine mobile Anwendung für Startup-Entwickler bestellen, um Ihr Projekt berühmter zu machen und das Vertrauen und die Loyalität der Zielgruppe zu gewinnen."
    }
  },
  ru: {
    header: {
      pathOne: "Заказывайте качественные приложения",
      pathTwo: "в компании",
      strong: "IUIC"
    },
    body: {
      firstTextParagraph: "Если вы ищете новые эффективные инструменты продаж, заказывайте разработку мобильных приложений в компании IUIC. Наши специалисты подготовят приложение, которое станет незаменимым помощником для ваших клиентов!",
      secondTextParagpaph: "Мы предлагаем разработку приложений для любых областей на платформах – iOS и Android, таких как: сфера услуг, e-commerce, онлайн-СМИ, издательства. Также мы рекомендуем заказать мобильное приложение авторам стартапов, чтобы сделать свой проект более известным, и завоевать доверие и лояльность целевой аудитории."
    }
  }
});

class AboutCaption extends Component {
  render() {
    strings.setLanguage(this.props.lang);
    return(
      <div className="about__container">
        <Container>
          <Row>
            <Col xs="12">
              <h2 className="about-captiion__header">
                {strings.header.pathOne} <br /> {strings.header.pathTwo} <strong>{strings.header.strong}</strong>
              </h2>
            </Col>
            <Col xs="12">
              <p className="about-captiion__text">
                {strings.body.firstTextParagraph}
              </p>
            </Col>
            <Col xs="12">
              <p className="about-captiion__text">
                {strings.body.secondTextParagpaph}
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default AboutCaption;