import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import Slider from 'react-slick';
import Card from '../Card';
import LocalizedStrings from 'react-localization';
import '../../styles/contacts.css';

  const strings = new LocalizedStrings({
    en: {
      ceo: "Igor",
      sm: "Vladislav",
      hr: "Maksim",
      ios: "Konstantin",
      android: "Maksim",
      back: "Denis",
      cm: "Evgeniy",
      design: "Valeriia"
    },
    de: {
      ceo: "Igor",
      sm: "Vladislav",
      hr: "Maksim",
      ios: "Konstantin",
      android: "Maksim",
      back: "Denis",
      cm: "Evgeniy",
      design: "Valeriia"
    },
    ru: {
      ceo: "Игорь",
      sm: "Владислав",
      hr: "Максим",
      ios: "Константин",
      android: "Максим",
      back: "Денис",
      cm: "Евгений",
      design: "Валерия"
    }
  });

function SampleNextArrow(props) {
  const { onClick } = props
  return (
    <div
      className="slider-button slider-button-next"
      onClick={onClick}>
      <FontAwesome name='chevron-right' className="next-arrow" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props
  return (
    <div
      className="slider-button slider-button-prev"
      onClick={onClick}>
      <FontAwesome name='chevron-left' className="prev-arrow" />
    </div>
  );
}

class Contacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slidesCount: 1
    }
  }

  componentDidMount() {
    let ww = window.innerWidth,
        wh = window.innerHeight,
        count = ww > 700 ? 7 : 1;

      if(ww < 500) {
        count = 1;
      } else if(ww < 790 && wh > 670) {
        count = 3;
      } else if(ww > 750 && ww < 1050) {
        count = 5;
      } else {
        count = 7;
      }

    this.setState({
      slidesCount: count
    })
  }

  render() {

    strings.setLanguage(this.props.lang);

    let settings = {
          centerMode: true,
          dots: false,
          infinite: true,
          speed: 500,
          slidesToScroll: 1,
          slidesToShow: this.state.slidesCount,
          swipeToSlide: true,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          autoplay: true
        };

    return(
      <div className="contacts" id="comand">
              <Slider {...settings}>
                <div>
                  <Card
                    position="Chief Executive Officer"
                    name={strings.ceo}
                    img="ceo">
                  </Card>
                </div>
                <div>
                  <Card
                    position="Sales Manager"
                    name={strings.sm}
                    img="sm">
                  </Card>
                </div>
                <div>
                  <Card
                    position="hr"
                    name={strings.hr}
                    img="hr">
                  </Card>
                </div>
                <div>
                  <Card
                    position="ios developer"
                    name={strings.ios}
                    img="ios">
                  </Card>
                </div>
                <div>
                  <Card
                    position="Android Developer"
                    name={strings.android}
                    img="android">
                  </Card>
                </div>
                <div>
                  <Card
                    position="Back-end developer"
                    name={strings.back}
                    img="back-end">
                  </Card>
                </div>
                <div>
                  <Card
                    position="Content Manager"
                    name={strings.cm}
                    img="cm">
                  </Card>
                </div>
                <div>
                  <Card
                    position="Designer"
                    name={strings.design}
                    img="designer">
                  </Card>
                </div>
              </Slider>
      </div>
    );
  }
}

export default Contacts;