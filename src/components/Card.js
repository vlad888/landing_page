import React, { Component } from 'react';
import '../styles/card.css';

class Card extends Component {
  render() {
    return (
      <div className="card-block__container">
        <div className="card-block__header">
          <div className={"card-block__img-container " + this.props.img }></div>
        </div>
        <div className="card-block__body">
          <div className="card-block__name">{this.props.name}</div>
          <div className="card-block__position">{this.props.position}</div>
        </div>
      </div>
    );
  }
}

export default Card;