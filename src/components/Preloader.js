import React, { Component } from 'react';
import LocalizedStrings from 'react-localization';
import { PulseLoader } from 'react-spinners';
import '../styles/preloader.css';

let strings = new LocalizedStrings({
  en: {
    loading: "Loading"
  },
  de: {
    loading: "Laden"
  },
  ru: {
    loading: "Загрузка"
  }
});

class Preloader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

  render() {
    return(
      <div className={this.props.className}>
        {strings.loading}
        <PulseLoader
          size={5}
          color="#fff"
          margin="0 5px">
        </PulseLoader>
      </div>
    );
  }
}

export default Preloader;