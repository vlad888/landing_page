import React, { Component } from 'react';
import { Container, Row, Col, Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Tooltip } from 'reactstrap';
import scrollToElement from 'scroll-to-element';
import LocalizedStrings from 'react-localization';
import '../../styles/cl-effect-21.css';

let strings = new LocalizedStrings({
  ru: {
    main: "Главная",
    servises: "Сервисы",
    portfolio: "Портфолио",
    contacts: "Контакты",
    team: "Команда"
  },
  en: {
    main: "Main",
    servises: "Services",
    portfolio: "Portfolio",
    contacts: "Contact",
    team: "Our team"
  },
  de: {
    main: "Home",
    servises: "Services",
    portfolio: "Portfolio",
    contacts: "Kontakt",
    team: "Team"
  }
});

class NavHeader extends Component {
  constructor(props) {
    super(props);

    this.scrollTo = this.scrollTo.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.toggle = this.toggle.bind(this);
    this.changeLang = this.changeLang.bind(this);

    this.state = {
      isOpen: false,
      tooltipOpen: false,
      fixed: false,
      activeLink: (() => {
        switch( strings.getInterfaceLanguage() ) {
          case 'en':
            this.props.langEn();
            return '/en'
          case 'ru':
            this.props.langRu();
            return '/ru'
          case 'de':
            this.props.langDe();
            return '/de'
          default:
            this.props.langEn();
            return '/en'
        }
      })(),
      langRu: "/ru",
      langEn: "/en",
      langDe: "/de"
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleTooltip() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {

    let scrolled = window.pageYOffset || document.documentElement.scrollTop,
      navbarOffsetTop = 40;

    if (scrolled > navbarOffsetTop && !this.state.fixed) {
      this.setState({
        fixed: true
      })
    }

    if (scrolled < navbarOffsetTop && this.state.fixed) {
      this.setState({
        fixed: false
      })
    }
  }

  scrollTo(event) {
    event.preventDefault();
    let id = event.target.hash;
    scrollToElement(id, {
      offset: -60,
      ease: 'linear',
      duration: 1000
    })
  }

  changeLang(event){
    event.preventDefault();
    this.setState({
      activeLink: event.target.pathname
    })

    switch(event.target.pathname) {
      case '/en':
        this.props.langEn();
        break;
      case '/ru':
        this.props.langRu();
        break;
      case '/de':
        this.props.langDe();
        break;
      default:
        this.props.langEn();
        break;
    }
  }

  render() {
    strings.setLanguage(this.props.lang);
    return(
      <div className={this.state.fixed ? "header-nav header-nav--fixed" : "header-nav"}>
        <Container >
          <Row>
            <Col xs="12" lg="9">
              <Navbar className="navbar-expand-lg navbar-light">
                <NavbarBrand href="/" id="main-logo-image">
                  <img className={this.state.fixed ? "header-nav__main-logo-image header-nav__main-logo-image--small" : "header-nav__main-logo-image"} src="/iuic-logo.png" alt="iuic-logo" />
                </NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Tooltip placement="right"
                  isOpen={this.state.tooltipOpen}
                  target="main-logo-image"
                  toggle={this.toggleTooltip.bind(this)}>
                  International​ ​Union​​ of​​ Information​​ Cities
                </Tooltip>
                <Collapse isOpen={this.state.isOpen} navbar>
                  <Nav className="ml-auto cl-effect-21" navbar>
                    <NavItem>
                      <NavLink href="#top"
                        onClick={this.scrollTo}>{strings.main}</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="#servises"
                        onClick={this.scrollTo}>{strings.servises}</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="#projects"
                        onClick={this.scrollTo}>{strings.portfolio}</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="#footer"
                        onClick={this.scrollTo}>{strings.contacts}</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="#comand"
                        onClick={this.scrollTo}>{strings.team}</NavLink>
                    </NavItem>
                    <NavItem className="display-xs">
                        <Nav className="lang-nav--collapse" navbar>
                          <NavItem>
                            <NavLink href="en" className={this.state.activeLink === this.state.langEn ? "lang-link active" : "lang-link"} onClick={this.changeLang}>EN</NavLink>
                          </NavItem>
                          <NavItem className="nav-item">
                            <NavLink href="de" className={this.state.activeLink === this.state.langDe ? "lang-link active" : "lang-link"} onClick={this.changeLang}>DE</NavLink>
                          </NavItem>
                          <NavItem className="nav-item">
                            <NavLink href="ru" className={this.state.activeLink === this.state.langRu ? "lang-link active" : "lang-link"} onClick={this.changeLang}>RU</NavLink>
                          </NavItem>
                        </Nav>
                    </NavItem>
                  </Nav>
                </Collapse>
              </Navbar>
            </Col>
            <Col xs="12" lg="3" className="hidden-xs">
              <Navbar className="navbar-expand-lg navbar-light">
                <Nav className="ml-auto cl-effect-10 lang-nav" navbar>
                  <NavItem>
                    <NavLink href="en" className={this.state.activeLink === this.state.langEn ? "lang-link active" : "lang-link"} onClick={this.changeLang}>EN</NavLink>
                  </NavItem>
                  <NavItem className="nav-item">
                    <NavLink href="de" className={this.state.activeLink === this.state.langDe ? "lang-link active" : "lang-link"} onClick={this.changeLang}>DE</NavLink>
                  </NavItem>
                  <NavItem className="nav-item">
                    <NavLink href="ru" className={this.state.activeLink === this.state.langRu ? "lang-link active" : "lang-link"} onClick={this.changeLang}>RU</NavLink>
                  </NavItem>
                </Nav>
              </Navbar>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default NavHeader;