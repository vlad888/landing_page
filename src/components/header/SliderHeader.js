import React, { Component } from 'react';
import Media from 'react-media';
import { Carousel } from 'react-responsive-carousel';
import { TweenMax } from 'gsap';
import LocalizedStrings from 'react-localization';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

let strings = new LocalizedStrings({
  ru: {
    yourIdea: "Превратим ваши идеи",
    firstSlide: {
      firstPath: "в уникальные мобильные",
      lastPath: "приложения"
    },
    secondSlide: {
      firstPath: "В многофункциональные",
      lastPath: "инструменты"
    },
    thirdSlide: {
      firstPath: "в популярные",
      lastPath: "интернет магазины"
    },
    fourSlide: {
      firstPath: "в эфективные",
      lastPath: "продажи"
    },
    fiveSlide: {
      firstPath: "в безграничные",
      lastPath: "возможности"
    }
  },
  en: {
    yourIdea: "Convert your ideas",
    firstSlide: {
      firstPath: "into unique mobile",
      lastPath: "applications"
    },
    secondSlide: {
      firstPath: "into multifunctional",
      lastPath: "tools"
    },
    thirdSlide: {
      firstPath: "into effective",
      lastPath: "online shops"
    },
    fourSlide: {
      firstPath: "into effective",
      lastPath: "sales"
    },
    fiveSlide: {
      firstPath: "into limitless",
      lastPath: "possibilities"
    }
  },
  de: {
    yourIdea: "Verwandeln Sie Ihre Ideen",
    firstSlide: {
      firstPath: "in einzigartigen mobilen",
      lastPath: "Anwendungen"
    },
    secondSlide: {
      firstPath: "in multifunktionalen ",
      lastPath: "Werkzeugen"
    },
    thirdSlide: {
      firstPath: "in den beliebten",
      lastPath: "Online-Shops"
    },
    fourSlide: {
      firstPath: "im effektiven",
      lastPath: "Verkauf"
    },
    fiveSlide: {
      firstPath: "in grenzenlosen",
      lastPath: "Möglichkeiten"
    }
  }
});

class SliderHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: 0,
      firstImgSlide: 0,
      secondImgSlide: 1,
      thirdImgSlide: 2,
      fourImgSlide: 3,
      fiveImgSlide: 4
    }
  }

  componentDidMount() {
    TweenMax.to(".bg1__animation--second", 1, {
      top: "45%",
      opacity: 1,
      delay: 1.5
    })
    TweenMax.to(".bg1__animation--third", 1, {
      top: "51%",
      opacity: 1,
      delay: 2
    })
    TweenMax.to(".bg1__animation--second, .bg1__animation--third", 1, {
      opacity: 0,
      delay: 3.5,
      onComplete: () => {
        let bg1AnimAll = document.querySelectorAll(".bg1__animation");
        bg1AnimAll.forEach( (item) => item.style = "" )
      }
    })
  }

  stopAnimation(elem) {
    let bgAnimAll = document.querySelectorAll(elem);

    bgAnimAll.forEach( (item) => {
      item.style = "";
    })

  }

  onChange(index) {
    this.setState({
      selectedItem: index
    })
    TweenMax.killAll();

    if (index === this.state.firstImgSlide) {

      TweenMax.to(".bg1__animation--second", 1, {
        top: "45%",
        opacity: 1,
        delay: 1.5
      })
      TweenMax.to(".bg1__animation--third", 1, {
        top: "51%",
        opacity: 1,
        delay: 2
      })
      TweenMax.to(".bg1__animation--second, .bg1__animation--third", 1, {
        opacity: 0,
        delay: 3.5,
        onComplete: () => {
          let bg1AnimAll = document.querySelectorAll(".bg1__animation");
          bg1AnimAll.forEach( (item) => item.style = "" )
        }
      })

      this.stopAnimation(".bg5__animation");
      this.stopAnimation(".bg2__animation");

    }

    if (index === this.state.secondImgSlide) {
      TweenMax.to(".bg2__animation--second", 1, {
        top: "45%",
        opacity: 1,
        delay: 1.5
      })
      TweenMax.to(".bg2__animation--third", 1, {
        top: "51%",
        opacity: 1,
        delay: 2
      })
      TweenMax.to(".bg2__animation--second, .bg2__animation--third", 1, {
        opacity: 0,
        delay: 3.5,
        onComplete: () => {
          let bg1AnimAll = document.querySelectorAll(".bg2__animation");
          bg1AnimAll.forEach( (item) => item.style = "" )
        }
      })

      this.stopAnimation(".bg1__animation");
      this.stopAnimation(".bg3__animation");

    }

    if (index === this.state.thirdImgSlide) {
      TweenMax.to(".bg3__animation--second", 1, {
        top: "45%",
        opacity: 1,
        delay: 1.5
      })
      TweenMax.to(".bg3__animation--third", 1, {
        top: "51%",
        opacity: 1,
        delay: 2
      })
      TweenMax.to(".bg3__animation--second, .bg3__animation--third", 1, {
        opacity: 0,
        delay: 3.5,
        onComplete: () => {
          let bg1AnimAll = document.querySelectorAll(".bg3__animation");
          bg1AnimAll.forEach( (item) => item.style = "" )
        }
      })

      this.stopAnimation(".bg2__animation");
      this.stopAnimation(".bg4__animation");

    }

    if (index === this.state.fourImgSlide) {
      TweenMax.to(".bg4__animation--second", 1, {
        top: "45%",
        scale: 1.1,
        opacity: 1,
        delay: 1.5
      })
      TweenMax.to(".bg4__animation--third", 1, {
        top: "51%",
        opacity: 1,
        scale: 1.3,
        delay: 2
      })
      TweenMax.to(".bg4__animation--second, .bg4__animation--third", 1, {
        opacity: 0,
        delay: 3.5,
        onComplete: () => {
          let bg1AnimAll = document.querySelectorAll(".bg4__animation");
          bg1AnimAll.forEach( (item) => item.style = "" )
        }
      })

      this.stopAnimation(".bg3__animation");
      this.stopAnimation(".bg5__animation");

    }

    if (index === this.state.fiveImgSlide) {
      TweenMax.to(".bg5__animation--second", 1, {
        top: "45%",
        opacity: 1,
        delay: 1.5
      })
      TweenMax.to(".bg5__animation--third", 1, {
        top: "51%",
        opacity: 1,
        delay: 2
      })
      TweenMax.to(".bg5__animation--second, .bg5__animation--third", 1, {
        opacity: 0,
        delay: 3.5,
        onComplete: () => {
          let bg1AnimAll = document.querySelectorAll(".bg5__animation");
          bg1AnimAll.forEach( (item) => item.style = "" )
        }
      })

      this.stopAnimation(".bg4__animation");
      this.stopAnimation(".bg1__animation");

    }
  }

  render() {
    strings.setLanguage(this.props.lang);
    const Slider = <div>
                    <div className="bg1__animation bg1__animation--first">{strings.yourIdea}</div>
                    <div className="bg1__animation bg1__animation--second">{strings.firstSlide.firstPath}</div>
                    <div className="bg1__animation bg1__animation--third">{strings.firstSlide.lastPath}</div>
                    <div className="bg2__animation bg2__animation--second">{strings.secondSlide.firstPath}</div>
                    <div className="bg2__animation bg2__animation--third">{strings.secondSlide.lastPath}</div>
                    <div className="bg3__animation bg3__animation--second">{strings.thirdSlide.firstPath}</div>
                    <div className="bg3__animation bg3__animation--third">{strings.thirdSlide.lastPath}</div>
                    <div className="bg4__animation bg4__animation--second">{strings.fourSlide.firstPath}</div>
                    <div className="bg4__animation bg4__animation--third">{strings.fourSlide.lastPath}</div>
                    <div className="bg5__animation bg5__animation--second">{strings.fiveSlide.firstPath}</div>
                    <div className="bg5__animation bg5__animation--third">{strings.fiveSlide.lastPath}</div>
                    <Carousel
                      axis="horizontal"
                      showThumbs={false}
                      showArrows={true}
                      showStatus={false}
                      showIndicators={false}
                      autoPlay={true}
                      interval={4000}
                      infiniteLoop={true}
                      onChange={this.onChange.bind(this)}
                      stopOnHover={false}
                      selectedItem={this.state.selectedItem}
                      className="header__slider slider"
                    >
                    <div className="slider__item">
                      <img className="slider__img" src="/slider/carousel-bg-1.png" alt="" />
                    </div>
                    <div className="slider__item">
                      <img className="slider__img" src="/slider/carousel-bg-2.png" alt="" />
                    </div>
                    <div className="slider__item">
                      <img className="slider__img" src="/slider/carousel-bg-3.jpg" alt="" />
                    </div>
                    <div className="slider__item">
                      <img className="slider__img" src="/slider/carousel-bg-4.png" alt="" />
                    </div>
                    <div className="slider__item">
                      <img className="slider__img" src="/slider/carousel-bg-5.png" alt="" />
                    </div>
                  </Carousel>
                </div>

    return (
      <Media query="(min-width: 767px)">
        {matches => matches ? (
          Slider
        ) : (
          null
        )}
      </Media>
    );
  }
}

export default SliderHeader;