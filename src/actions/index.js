export const CHANGED_LANG_TO_RU = "CHANGED_LANG_TO_RU";
export const CHANGED_LANG_TO_EN = "CHANGED_LANG_TO_EN";
export const CHANGED_LANG_TO_DE = "CHANGED_LANG_TO_DE";

export function changedLangToRu() {
  return {
    type: CHANGED_LANG_TO_RU,
    payload: "ru"
  }
}

export function changedLangToEn() {
  return {
    type: CHANGED_LANG_TO_EN,
    payload: "en"
  }
}

export function changedLangToDe() {
  return {
    type: CHANGED_LANG_TO_DE,
    payload: "de"
  }
}