import React, { Component } from 'react';
import Servises from '../components/main/Servises';
import AboutCaption from '../components/main/AboutCaption';
import Projects from '../components/main/Projects';
import Feedback from '../components/main/Feedback';
import Contacts from '../components/main/Contacts';

class Main extends Component {
  render() {
    return(
      <main className="main">
        <Servises
          lang={this.props.lang}>
        </Servises>
        <AboutCaption
          lang={this.props.lang}>
        </AboutCaption>
        <Projects
          lang={this.props.lang}>
        </Projects>
        <Feedback
          lang={this.props.lang}>
        </Feedback>
        <Contacts
          lang={this.props.lang}>
        </Contacts>
      </main>
    );
  }
}

export default Main;