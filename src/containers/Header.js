import React, { Component } from 'react';
import NavHeader from '../components/header/NavHeader';
import SliderHeader from '../components/header/SliderHeader';

class Header extends Component {
  render() {
    return(
      <header className="header" id="top">
        <NavHeader
          lang={this.props.lang}
          langRu={this.props.langRu}
          langEn={this.props.langEn}
          langDe={this.props.langDe}>
        </NavHeader>
        <SliderHeader
          lang={this.props.lang}></SliderHeader>
      </header>
    );
  }
}

export default Header;