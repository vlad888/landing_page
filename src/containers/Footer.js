import React, { Component } from 'react';
import Content from '../components/footer/Content';
import '../styles/footer.css';

class Footer extends Component {
  render() {
    return(
      <footer className="footer" id="footer">
        <Content />
      </footer>
    );
  }
}

export default Footer;