import {
  CHANGED_LANG_TO_RU,
  CHANGED_LANG_TO_EN,
  CHANGED_LANG_TO_DE
} from '../actions';

const initialState = {
  lang: "ru"
};

export default function appState(state = initialState, action) {

  switch(action.type) {
    case CHANGED_LANG_TO_RU:
    case CHANGED_LANG_TO_EN:
    case CHANGED_LANG_TO_DE:
      return { ...state,
        lang: action.payload
      }

    default:
      return state;
  }
}