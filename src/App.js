import React, { Component } from 'react';
import Preloader from './components/Preloader';
import Header from './containers/Header';
import Main from './containers/Main';
import Footer from './containers/Footer';
import FeedbackModal from './components/modals/FeedbackModal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from './actions';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      show: true
    }
  }

  componentDidMount() {
    let _this = this;
    window.addEventListener("load", function() {
      _this.setState({
        loading: !_this.state.loading
      });
      setTimeout( () => {
        _this.setState({
          show: !_this.state.show
        });
      }, 1000)
    })
  }
  render() {
    return (
      <div className="index-page">
        { this.state.show ? <Preloader
          lang={this.props.lang}
          className={this.state.loading ? "preloader" : "preloader preloader--hide"}>
        </Preloader> : null }
        <Header
          lang={this.props.lang}
          langRu={this.props.actions.changedLangToRu}
          langEn={this.props.actions.changedLangToEn}
          langDe={this.props.actions.changedLangToDe}>
        </Header>
        <Main
          lang={this.props.lang}>
        </Main>
        <Footer
          lang={this.props.lang}>
        </Footer>
        <FeedbackModal
          lang={this.props.lang}>
        </FeedbackModal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.lang
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);